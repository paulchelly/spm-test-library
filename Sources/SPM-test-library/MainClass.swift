//
//  File.swift
//  
//
//  Created by Pavel Ivanov on 20.05.2020.
//

import Foundation

public class MainClassPublic {
    static let myBeatifullClassDesc = "Hello Public"
}

class MainClassDefault {
    static let myBeatifullClassDesc = "Hello Default"
}
