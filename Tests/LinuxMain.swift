import XCTest

import SPM_test_libraryTests

var tests = [XCTestCaseEntry]()
tests += SPM_test_libraryTests.allTests()
XCTMain(tests)
